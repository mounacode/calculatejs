import 'bootstrap/dist/css/bootstrap.min.css';
import { useState } from 'react';
import './App.css';

function App() {
  const [expression, setExpresion] = useState("");
  const [answer, setAnswer]= useState(0)

 

   const display=(symbol)=>{
    setExpresion(prev => prev +symbol)
    if(expression[expression.length - 1] == "="){
      if(/[1-9.]/.test(symbol)){
         setExpresion(symbol)
      }else{
        setExpresion( answer + symbol)
      }

    }
   }
   const calculate=()=>{
    setAnswer(eval(expression))
    setExpresion((prev) => prev + "=") 
   }
   const allClear=()=>{
    setExpresion('')
    setAnswer(0)
   }
   const clear=()=>{
      setExpresion(prev => prev.split("").slice(0, prev.length - 1).join(""))
      setAnswer(0)
   }
  return (
    <div className="container">
      <div className="grid">
        <div className=" dis">
          <input type="text" value={expression} placeholder ='0' disabled />
          
        <div className="total">{answer}</div>
        </div>
        <div onClick={ ()=> allClear()} className="padButton A  tomato">AC</div>
        <div onClick= { ()=> clear() } className="padButton C tomato">C</div>
        <div  onClick={()=>display("/")}className="padButton div">/</div>
        <div onClick={ ()=> display ("*")} className="padButton times ">x</div>
        <div onClick={()=>display("7")} className="padButton seven dark-gary">7</div>
        <div  onClick={ ()=> display("8")}className="padButton height dark-gary">8</div>
        <div onClick={ ()=> display("9")} className="padButton nine  dark-gary">9</div>
        <div  onClick={ ()=> display("-")}className="padButton minus">-</div>
        <div  onClick={ ()=> display("4")}className="padButton four dark-gary">4</div>
        <div  onClick={ ()=> display("5")}className="padButton five dark-gary">5</div>
        <div onClick={ ()=> display ("6")} className="padButton six dark-gary">6</div>
        <div onClick={ ()=> display("+")} className="padButton plus">+</div>
        <div onClick={ ()=> display("1")} className="padButton one dark-gary">1</div>
        <div onClick={ ()=> display("2")} className="padButton two dark-gary">2</div>
        <div onClick={ ()=> display("3")} className="padButton three dark-gary">3</div>
        <div  onClick={ ()=> calculate()}className="padButton equal blue">=</div>
        <div onClick={ ()=> display("0")} className="padButton zero dark-gary">0</div>
        <div  onClick={ ()=> display(".")}className="padButton dot dark-gary">.</div>
      </div>
      
    
    </div>
  );
}

export default App;
